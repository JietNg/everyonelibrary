package com.example.nghsiaojiet.everyonelibrary;

import android.app.Application;

public class Global extends Application {
    private int Id;
    private Profile objProfile;


    public Profile getObjProfile() {
        return objProfile;
    }

    public void setObjProfile(Profile objProfile) {
        this.objProfile = objProfile;
    }

    public int getId(){
        return Id;
    }
    public void setId(int id){
        this.Id = id;
    }
}
