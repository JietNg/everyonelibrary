package com.example.nghsiaojiet.everyonelibrary;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class BookDetails extends AppCompatActivity {
    ImageButton imgBookPhoto;
    TextView txtTitle;
    TextView txtAuthor;
    TextView txtDate;
    TextView txtBookCat;
    TextView txtDesc;
    Button btnDonate;
    DatabaseHelper db;
    List<Book> BookList = new ArrayList<Book>();
    int Id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);
        imgBookPhoto = findViewById(R.id.imgBookPhoto);
        txtTitle = findViewById(R.id.txtTitle);
        txtAuthor = findViewById(R.id.txtAuthor);
        txtDate = findViewById(R.id.txtDate);
        txtDesc = findViewById(R.id.txtDesc);
        txtBookCat = findViewById(R.id.txtBookCat);
        btnDonate = findViewById(R.id.btnDonate);
        Bundle extras = getIntent().getExtras();
        Id = extras.getInt("Id");
        Log.d("Id", "onCreate: " + Id);
        db = new DatabaseHelper(this);
        Book objBook = db.getBookById(Id);
        if(objBook == null){
            Log.d("Check", "onCreate: objBook is Null");
        }
        else{
            Log.d("check", "onCreate: objBook is not Null");
            Bitmap bit = BitmapFactory.decodeByteArray(objBook.getBookPhoto(), 0, objBook.getBookPhoto().length);
            imgBookPhoto.setImageBitmap(Bitmap.createScaledBitmap(bit, 500, 500, false));
            txtTitle.setText(objBook.getTitle());
            txtAuthor.setText(objBook.getAuthor());
            txtDesc.setText(objBook.getDesc());
            txtDate.setText(objBook.getDate());
            txtBookCat.setText(objBook.getBookCat());
        }

        btnDonate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BookDetails.this, CreditCardForm.class);
                startActivity(intent);
            }
        });


    }


}
