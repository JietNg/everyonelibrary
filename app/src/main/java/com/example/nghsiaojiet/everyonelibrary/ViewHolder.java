package com.example.nghsiaojiet.everyonelibrary;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewHolder extends RecyclerView.ViewHolder {
    public ImageView imageView;
    public TextView txtTitle;
    public TextView txtAuthor;
    public Button btnDetails;
    public View v;

    public ViewHolder(View itemView) {
        super(itemView);
        v = itemView;
        imageView = v.findViewById(R.id.imgBookPhoto);
        txtTitle = v.findViewById(R.id.txtTitle);
        txtAuthor = v.findViewById(R.id.txtAuthor);
        btnDetails = v.findViewById(R.id.btnDetails);

    }
}
