package com.example.nghsiaojiet.everyonelibrary;

import java.sql.Blob;

public class Profile {
    public int Id;
    public String Name;
    public byte[] Photo;
    public String Email;
    public String PhoneNo;
    public String Password;
    public int Appreciation;



    public Profile(String name, byte[] photo, String email, String phoneNo, String password, int appreciation) {
        Name = name;
        Photo = photo;
        Email = email;
        PhoneNo = phoneNo;
        Password = password;
        Appreciation = appreciation;
    }

    public Profile(String name, String password) {
        Name = name;
        Password = password;
    }



    public Profile(String name, byte[] photo, String email, String phoneNo, String password) {
        Name = name;
        Photo = photo;
        Email = email;
        PhoneNo = phoneNo;
        Password = password;
    }

    public Profile() {
    }


    public String getPassword() { return Password; }

    public void setPassword(String password) { Password = password; }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public byte[] getPhoto() {
        return Photo;
    }

    public void setPhoto(byte[] photo) {
        Photo = photo;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

    public int getAppreciation() {
        return Appreciation;
    }

    public void setAppreciation(int appreciation) {
        Appreciation = appreciation;
    }

    public Profile(int id, String name, byte[] photo, String email, String phoneNo, String password) {
        Id = id;
        Name = name;
        Photo = photo;
        Email = email;
        PhoneNo = phoneNo;
        Password = password;
    }


}
