package com.example.nghsiaojiet.everyonelibrary;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Home extends AppCompatActivity {
    SliderAdapter sliderAdapter;
    TextView txtDonationsMade;
    ViewPager viewPager;
    LinearLayout mDotLayout;
    TextView[] mDots;
    Profile objProfile;
    DatabaseHelper db;
    Global global;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        sliderAdapter = new SliderAdapter(this);
        txtDonationsMade = findViewById(R.id.txtDonationsMade);
        viewPager = findViewById(R.id.slideViewPager);
        mDotLayout = findViewById(R.id.dotsLayout);
        global = ((Global)getApplicationContext());
        db = new DatabaseHelper(this);
        objProfile = db.getProfileDetailsById(global.getId());
        Log.d("Check the appreciation", "onCreate: " + objProfile.getAppreciation() + " And " + objProfile.getEmail());
        viewPager.setAdapter(sliderAdapter);

        addDotsIndicator(0);

        viewPager.addOnPageChangeListener(viewListener);

    }

    public void addDotsIndicator(int position){
        mDots = new TextView[2];
        mDotLayout.removeAllViews();
        for (int i = 0 ; i < mDots.length; i++){
            mDots[i] = new TextView(this);
            mDots[i].setText((Html.fromHtml("&#8226;")));
            mDots[i].setTextSize(35);
            mDots[i].setTextColor(getResources().getColor(R.color.colorTransparentWhite));

            mDotLayout.addView(mDots[i]);
        }

        //change selected to white
        if (mDots.length > 0){
            mDots[position].setTextColor(getResources().getColor(R.color.windowBackground));
        }
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            addDotsIndicator(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}
