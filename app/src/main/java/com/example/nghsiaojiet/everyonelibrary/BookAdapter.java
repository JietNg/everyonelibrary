package com.example.nghsiaojiet.everyonelibrary;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<ViewHolder> {
    public List<Book> BookList = new ArrayList<Book>();
    public Context context;
    private Cursor mCursor;

    public BookAdapter(List<Book> bookList, Context context){
        BookList = bookList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_item_layout, parent, false);
        return new ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        DatabaseHelper db = new DatabaseHelper(context);
        mCursor = db.getAllBooks();
        mCursor.moveToPosition(position);

        int ID = 0;
        String Title = null;
        String Author = null;
        String Desc = null;
        String Date = null;
        String BookCat = null;
        byte[] BookPhoto = null;
        while (mCursor.moveToNext()){
            ID = mCursor.getInt(0);
            Title = mCursor.getString(1);
            Author = mCursor.getString(2);
            Desc = mCursor.getString(3);
            Date = mCursor.getString(4);
            BookCat = mCursor.getString(5);
            BookPhoto = mCursor.getBlob(6);
            BookList.add(new Book(ID, Title, Author, Desc, Date, BookCat, BookPhoto));
        }
        Book b = BookList.get(position);
        Bitmap bit = BitmapFactory.decodeByteArray(b.getBookPhoto(), 0, b.getBookPhoto().length);
        holder.txtTitle.setText(b.getTitle());
        holder.txtAuthor.setText(b.getAuthor());
        Log.d("Check null", "onBindViewHolder: " + b.getTitle()+" " + b.getAuthor());
        if(b.getBookPhoto() == null){
            holder.imageView.setImageResource(R.drawable.bookcoverpage);
        }
        else{
            holder.imageView.setImageBitmap(Bitmap.createScaledBitmap(bit, 500, 500, false));
        }
        holder.btnDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = holder.getAdapterPosition();
                Log.d("Details", "More details button clicked");
                Intent intent = new Intent(context, BookDetails.class);
                mCursor.moveToPosition(position);
                intent.putExtra("Id", mCursor.getInt(0));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return BookList.size();
    }

    public Cursor swapCursor(Cursor c){
        if(mCursor == c){
            return null;
        }
        Cursor temp = mCursor;
        this.mCursor = c;
        if(c!= null){
            this.notifyDataSetChanged();
        }
        return temp;
    }

    public void clear() {
        final int size = BookList.size();
        BookList.clear();
        notifyItemRangeRemoved(0, size);
    }
}
