package com.example.nghsiaojiet.everyonelibrary;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;

public class CreateBook extends AppCompatActivity {
    EditText txtTitle;
    EditText txtAuthor;
    EditText txtDetails;
    Spinner catSpinner;
    ImageButton imgPicture;
    CircularProgressButton btnCreate;
    DatabaseHelper db;
    Bitmap thumbnail;
    public static final int GET_FROM_GALLERY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_book);
        txtTitle = findViewById(R.id.txtTitle);
        txtAuthor = findViewById(R.id.txtAuthor);
        txtDetails = findViewById(R.id.txtDetails);
        catSpinner = findViewById(R.id.catSpinner);
        imgPicture = findViewById(R.id.imgPicture);
        btnCreate = findViewById(R.id.btnCreate);


        imgPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    if(ActivityCompat.checkSelfPermission(CreateBook.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                        imgPicture.setEnabled(false);
                        ActivityCompat.requestPermissions(CreateBook.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, GET_FROM_GALLERY);
                    }
                    else{
                        imgPicture.setEnabled(true) ;
                        startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI), GET_FROM_GALLERY);
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnCreate.startAnimation();
                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                String formattedDate = df.format(c);
                db = new DatabaseHelper(CreateBook.this);
                imgPicture.setDrawingCacheEnabled(true);
                imgPicture.buildDrawingCache();
                Bitmap bitmap = imgPicture.getDrawingCache();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] data = baos.toByteArray();
                Book objBook = new Book(txtTitle.getText().toString(), txtAuthor.getText().toString(), txtDetails.getText().toString(), formattedDate, catSpinner.getSelectedItem().toString(), data);
                db.addBook(objBook);
                btnCreate.doneLoadingAnimation(R.color.colorblue, BitmapFactory.decodeResource(getResources(), R.drawable.tick));
                Toast.makeText(CreateBook.this, "Book has been created!", Toast.LENGTH_SHORT).show();
            }
        });




    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        //Detects request codes
        if(requestCode==GET_FROM_GALLERY && resultCode == Activity.RESULT_OK && null!= data) {
            try{
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                imgPicture.setImageBitmap(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults)
    {
        switch (requestCode) {
            case GET_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    imgPicture.setEnabled(true);
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, GET_FROM_GALLERY);
                } else {
                    Toast.makeText(CreateBook.this, "There seem to be a problem accessing the gallery", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    /*public void addToDb(View view){
        imgPicture.setDrawingCacheEnabled(true);
        imgPicture.buildDrawingCache();
        Bitmap bitmap = imgPicture.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        DatabaseHelper db = new DatabaseHelper(CreateBook.this);
        db.addBookPhoto(data);
        Toast.makeText(this, "Image saved successfully", Toast.LENGTH_SHORT).show();
    }*/


}
