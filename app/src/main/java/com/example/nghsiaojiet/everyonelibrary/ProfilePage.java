package com.example.nghsiaojiet.everyonelibrary;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;

public class ProfilePage extends AppCompatActivity {
    ImageButton imgProfilePhoto;
    EditText txtName;
    EditText txtPhoneNo;
    EditText txtEmail;
    CircularProgressButton btnSave;
    RadioGroup rg;
    RadioButton rb;
    RadioButton RadYes;
    RadioButton RadNo;
    public static final int GET_FROM_GALLERY = 1;
    DatabaseHelper db;
    int Id;
    Global mGlobal;
    int appreciation;
    public String TAG = "ProfilePage";
    boolean checkimgButtonclick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_page);
        imgProfilePhoto = findViewById(R.id.imgProfilePhoto);
        txtName = findViewById(R.id.txtName);
        txtPhoneNo = findViewById(R.id.txtPhoneNo);
        txtEmail = findViewById(R.id.txtEmail);
        btnSave = findViewById(R.id.btnSave);
        rg = findViewById(R.id.RadGroup);
        RadNo = findViewById(R.id.RadNo);
        RadYes = findViewById(R.id.RadYes);
        mGlobal = ((Global)getApplicationContext());
        Id = mGlobal.getId();
        checkimgButtonclick = false;
        Log.d("Display Id", "onCreate: " + Id );
        db = new DatabaseHelper(ProfilePage.this);
        Profile objProfile = new Profile();
        objProfile = db.getProfileDetailsById(Id);
        final Bitmap bit = BitmapFactory.decodeByteArray(objProfile.getPhoto(), 0, objProfile.getPhoto().length);
        imgProfilePhoto.setImageBitmap(Bitmap.createScaledBitmap(bit, 500, 500, false));
        txtName.setText(objProfile.getName());
        txtEmail.setText(objProfile.getEmail());
        txtPhoneNo.setText(objProfile.getPhoneNo());
        appreciation = objProfile.getAppreciation(); // always 0
        Log.d("Display boolean", "onCreate: " + appreciation);
        if(appreciation == 1){
            Log.d(TAG, "onCreate: Yes is checked");
            rg.check(((RadioButton)rg.getChildAt(0)).getId());
        }
        else{
            Log.d(TAG, "onCreate: No is checked");
            rg.check(((RadioButton)rg.getChildAt(1)).getId());
        }

        imgProfilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    if(ActivityCompat.checkSelfPermission(ProfilePage.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                        imgProfilePhoto.setEnabled(false);
                        ActivityCompat.requestPermissions(ProfilePage.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, GET_FROM_GALLERY);
                    }
                    else{
                        imgProfilePhoto.setEnabled(true) ;
                        startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI), GET_FROM_GALLERY);
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                checkimgButtonclick = true;
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSave.startAnimation();
                db = new DatabaseHelper(ProfilePage.this);
                Profile objProfile = new Profile();
                imgProfilePhoto.setDrawingCacheEnabled(true);
                imgProfilePhoto.buildDrawingCache();
                Bitmap bitmap = imgProfilePhoto.getDrawingCache();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] data = baos.toByteArray();
                objProfile = db.getProfileDetailsById(Id);
                int radId = rg.getCheckedRadioButtonId();
                rb = findViewById(radId); //radio button that is checked
                CharSequence check = rb.getText();
                if(checkimgButtonclick == false){
                    Log.d(TAG, "onClick: No change in photo");
                    if(check.toString() == "Y"){
                        Log.d(TAG, "onClick: button Y is clicked");
                        db.updateProfileDetails(Id, txtName.getText().toString(), txtEmail.getText().toString(), txtPhoneNo.getText().toString(), 1);
                    }
                    else{
                        Log.d(TAG, "onClick: button N is clicked");
                        db.updateProfileDetails(Id, txtName.getText().toString(), txtEmail.getText().toString(), txtPhoneNo.getText().toString(), 0);
                    }
                    
                }
                else{
                    Log.d(TAG, "onClick: change in photo");
                    if(check.toString() == "Y"){
                        Log.d(TAG, "onClick: button Y is clicked");
                        db.updateProfilePhoto(Id, txtName.getText().toString(), data, txtEmail.getText().toString(), txtPhoneNo.getText().toString(), 1);
                    }
                    else{
                        Log.d(TAG, "onClick: button N is clicked");
                        db.updateProfilePhoto(Id, txtName.getText().toString(), data, txtEmail.getText().toString(), txtPhoneNo.getText().toString(), 0);
                    }
                    
                }
                btnSave.doneLoadingAnimation(R.color.colorblue, BitmapFactory.decodeResource(getResources(), R.drawable.tick));
                Toast.makeText(ProfilePage.this, "Profile has been updated! Refresh page to see changes", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        //Detects request codes
        if(requestCode==GET_FROM_GALLERY && resultCode == Activity.RESULT_OK && null!= data) {
            try{
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                imgProfilePhoto.setImageBitmap(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults)
    {
        switch (requestCode) {
            case GET_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    imgProfilePhoto.setEnabled(true);
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, GET_FROM_GALLERY);
                } else {
                    Toast.makeText(ProfilePage.this, "There seem to be a problem accessing the gallery", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void rbClick(View v){
        int radId = rg.getCheckedRadioButtonId();
        rb = findViewById(radId);
        Toast.makeText(this, "Selected button is: " + rb.getText(), Toast.LENGTH_SHORT).show();
    }
}
