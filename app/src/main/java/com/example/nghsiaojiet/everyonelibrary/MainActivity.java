package com.example.nghsiaojiet.everyonelibrary;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.Firebase;

public class MainActivity extends AppCompatActivity {
    EditText txtEmail;
    EditText txtPassword;
    Button btnlogin;
    Button btnForgetPass;
    Button btnSignUp;
    DatabaseHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Firebase.setAndroidContext(this);
        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);
        btnlogin = findViewById(R.id.btnLogin);
        btnForgetPass = findViewById(R.id.btnForgetPass);
        btnSignUp = findViewById(R.id.btnSignUp);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SignUp.class);
                startActivity(intent);
            }
        });
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db = new DatabaseHelper(MainActivity.this);
                Boolean check = db.checkProfileExist(txtEmail.getText().toString(), txtPassword.getText().toString());
                if(check){ //profile is not null
                    Profile objProfile = new Profile();
                    objProfile = db.getProfile(txtEmail.getText().toString(), txtPassword.getText().toString());
                    Global mGlobal = ((Global)getApplicationContext());
                    mGlobal.setId(objProfile.getId());
                    mGlobal.setObjProfile(objProfile);
                    Log.d("Check appreciation", "onClick: " + objProfile.getAppreciation()); //still 1 so appreciation value is correct
                    Intent intent = new Intent(MainActivity.this, Home.class);
                    startActivity(intent);
                }
                else{
                    Toast.makeText(MainActivity.this, "You have entered the wrong Email address/Password, please try again!", Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}
