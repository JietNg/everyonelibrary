package com.example.nghsiaojiet.everyonelibrary;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Color;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

public class Donation extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    MaterialSearchView barSearch;
    Toolbar toolbar;
    RecyclerView recyclerView;
    private ArrayList<Book> BookList = new ArrayList<Book>();
    DatabaseHelper db;
    private static final int IMAGES_LOADER= 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donation);
        barSearch = findViewById(R.id.BarSearch);
        toolbar = findViewById(R.id.toolbar);
        recyclerView = findViewById(R.id.recyclerView);
        db = new DatabaseHelper(Donation.this);
        AddBookList(BookList);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Donation");
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));

        final BookAdapter adapter = new BookAdapter(BookList, Donation.this);
        recyclerView.setLayoutManager(new LinearLayoutManager(Donation.this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        getLoaderManager().initLoader(IMAGES_LOADER, null, this);

        barSearch.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                Log.d("Donation", "onSearchViewShown: When search button is pressed");
                adapter.clear();
                AddBookList(BookList);
                BookAdapter adapter = new BookAdapter(BookList, Donation.this);
                recyclerView.setLayoutManager(new LinearLayoutManager(Donation.this));
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onSearchViewClosed() {
                Log.d("Donation", "onSearchViewClosed: When search button closed");
                adapter.clear();
                AddBookList(BookList);
                BookAdapter adapter = new BookAdapter(BookList, Donation.this);
                recyclerView.setLayoutManager(new LinearLayoutManager(Donation.this));
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(adapter);

            }
        });

        barSearch.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.clear();
                AddBookList(BookList);
                if(newText != null && !newText.isEmpty()){
                    List<Book> foundBook = new ArrayList<Book>();
                    for(Book item:BookList){
                       if(item.getTitle().contains(newText.toLowerCase()) || item.getTitle().contains(newText.toUpperCase())){
                           Log.d("Donation", "onQueryTextChange: A letter added in searchview and matches a letter");
                           foundBook.add(item);
                           BookAdapter adapter = new BookAdapter(foundBook, Donation.this);
                           recyclerView.setLayoutManager(new LinearLayoutManager(Donation.this));
                           recyclerView.setItemAnimator(new DefaultItemAnimator());
                           recyclerView.setAdapter(adapter);
                       }
                       else{
                           Log.d("Donation", "onQueryTextChange: A letter added in searchview but doesnt match a letter");
                           BookAdapter adapter = new BookAdapter(BookList, Donation.this);
                           recyclerView.setLayoutManager(new LinearLayoutManager(Donation.this));
                           recyclerView.setItemAnimator(new DefaultItemAnimator());
                           recyclerView.setAdapter(adapter);
                       }
                    }
                }
                return true;
            }
        });




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchmenu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        barSearch.setMenuItem(item);
        return true;
    }

    public Loader<Cursor> onCreateLoader(int i, Bundle bundle){
        String[] projection = {
                DatabaseHelper.COL_BookPhoto,
        };
        return new CursorLoader(this, ImagesProvider.CONTENT_URI, projection, null,null,null);
    }
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor){
        BookAdapter objadapter = new BookAdapter(BookList, Donation.this);
        objadapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        BookAdapter objadapter = new BookAdapter(BookList, Donation.this);
        objadapter.swapCursor(null);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    private void AddBookList(ArrayList list){
        int BookId = 0;
        String Title = null;
        String Author = null;
        String Description = null;
        String Date = null;
        String BookCat = null;
        byte[] BookPhoto = null;
        DatabaseHelper db = new DatabaseHelper(this);
        Cursor res = db.getAllBooks();
        while(res.moveToNext()){
            BookId = res.getInt(0);
            Title = res.getString(1);
            Author = res.getString(2);
            Description = res.getString(3);
            Date = res.getString(4);
            BookCat = res.getString(5);
            BookPhoto = res.getBlob(6);
            Book objBook = new Book(BookId, Title, Author, Description, Date, BookCat, BookPhoto);
            BookList.add(objBook);
        }
    }




}
