package com.example.nghsiaojiet.everyonelibrary;


import java.math.BigDecimal;
import java.sql.Blob;

public class Book {
    public int BookId;
    public String Title;
    public String Author;
    public String Desc;
    public String Date;
    public String BookCat;
    public byte[] BookPhoto;

    public Book(int bookId, String title, String author, String desc, String date, String bookCat, byte[] bookPhoto) {
        BookId = bookId;
        Title = title;
        Author = author;
        Desc = desc;
        Date = date;
        BookCat = bookCat;
        BookPhoto = bookPhoto;
    }

    public Book(String title, String author, String desc, String date, String bookCat, byte[] bookPhoto) {
        Title = title;
        Author = author;
        Desc = desc;
        Date = date;
        BookCat = bookCat;
        BookPhoto = bookPhoto;
    }

    public String getBookCat() {
        return BookCat;
    }

    public void setBookCat(String bookCat) {
        BookCat = bookCat;
    }

    public byte[] getBookPhoto() {
        return BookPhoto;
    }

    public void setBookPhoto(byte[] bookPhoto) {
        BookPhoto = bookPhoto;
    }

    public int getId() {
        return BookId;
    }

    public void setId(int id) {
        BookId = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }


    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }



    public Book() {
    }

    public Book(String title, String author, String desc, String date, String bookCat) {
        Title = title;
        Author = author;
        Desc = desc;
        Date = date;
        BookCat = bookCat;
    }
}
