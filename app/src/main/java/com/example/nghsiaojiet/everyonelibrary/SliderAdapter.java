package com.example.nghsiaojiet.everyonelibrary;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

public class SliderAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;

    public SliderAdapter(Context context) {
        this.context = context;
    }

    public int[] slide_images= {
            R.drawable.ic_home,
            R.drawable.ic_book,
            R.drawable.ic_input,
            R.drawable.ic_alarm,
            R.drawable.ic_account_circle_white
    };
    public String[] slide_headings= {
            "Book Donation",
            "Book Request",
            "Create Book",
            "Notification",
            "Profile"
    };
    @Override
    public int getCount() {
        return 2; //number of pages
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == (LinearLayout) object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout_fragment1, container, false);
        final CircleImageView btnAdd = (CircleImageView)view.findViewById(R.id.btnAddProfile);
        final CircleImageView btnAdd1 = view.findViewById(R.id.btnAdd1);
        final CircleImageView btnAdd2 = view.findViewById(R.id.btnAdd2);
        TextView tv1 = view.findViewById(R.id.slideDesc1);
        TextView tv2 = view.findViewById(R.id.slideDesc2);
        TextView tv3 = view.findViewById(R.id.slideDesc3);
        final int cPos = position;
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cPos == 0){ //first page
                    Intent gotoDonation = new Intent(context, Donation.class);
                    context.startActivity(gotoDonation);
                }
                else if(cPos == 1){ //second page
                    /*Intent gotoNotification = new Intent(context, Notification.class);
                    context.startActivity(gotoNotification);*/
                }

            }
        });
        btnAdd1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cPos == 0){
                    /*Intent gotoRequest = new Intent(context, Request.class);
                    context.startActivity(gotoRequest);*/
                }
                else if(cPos == 1){
                    Intent gotoProfile = new Intent(context, ProfilePage.class);
                    context.startActivity(gotoProfile);
                }
            }
        });
        btnAdd2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cPos == 0){
                    Intent gotoCreate = new Intent(context, CreateBook.class);
                    context.startActivity(gotoCreate);
                }
            }
        });
        if(cPos == 0){
            btnAdd.setImageResource(slide_images[0]);
            btnAdd1.setImageResource(slide_images[1]);
            btnAdd2.setImageResource(slide_images[2]);

            tv1.setText(slide_headings[0]);
            tv2.setText(slide_headings[1]);
            tv3.setText(slide_headings[2]);
        }
        else{
            btnAdd.setImageResource(slide_images[3]);
            btnAdd1.setImageResource(slide_images[4]);
            btnAdd2.setImageResource(R.color.colorblue);

            tv1.setText(slide_headings[3]);
            tv2.setText(slide_headings[4]);
            tv3.setText(null);
        }
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout)object);
    }
}


