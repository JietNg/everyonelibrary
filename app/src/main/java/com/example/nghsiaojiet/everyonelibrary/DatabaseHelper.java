package com.example.nghsiaojiet.everyonelibrary;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.support.constraint.Constraints.TAG;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "EveryOneLibraryDB.db";
    //book table
    public static final String BOOK_TABLE_NAME = "Book_Table";
    public static final String COL_BookId = "BookId";
    public static final String COL_Title = "Title";
    public static final String COL_Author = "Author";
    public static final String COL_Description = "Description";
    public static final String COL_Date = "Date";
    public static final String COL_BookCat = "BookCat";
    public static final String COL_BookPhoto = "BookPhoto";

    //profile table
    public static final String PROFILE_TABLE_NAME = "Profile_Table";
    public static final String COL_ProfileId = "ProfileId";
    public static final String COL_Name = "Name";
    public static final String COL_Photo = "Photo";
    public static final String COL_Email = "Email";
    public static final String COL_PhoneNo = "PhoneNo";
    public static final String COL_Password = "Password";
    public static final String COL_Appreciation = "Appreciation";

    public SQLiteDatabase db;
    Context context;
    ContentResolver mContentResolver;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        mContentResolver = context.getContentResolver();
        db = this.getWritableDatabase();
    }

    /*public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }*/


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + BOOK_TABLE_NAME + "(" + COL_BookId + " INTEGER PRIMARY KEY AUTOINCREMENT , " + COL_Title + " TEXT, " + COL_Author + " TEXT, " + COL_Description + " TEXT, " + COL_Date + " TEXT, " + COL_BookCat + " TEXT, " + COL_BookPhoto + " BLOB NOT NULL);");
        sqLiteDatabase.execSQL("CREATE TABLE " + PROFILE_TABLE_NAME + "(" + COL_ProfileId + " INTEGER PRIMARY KEY AUTOINCREMENT , " + COL_Name + " TEXT, " + COL_Photo + " BLOB NOT NULL, " + COL_Email + " TEXT, " + COL_PhoneNo + " TEXT, " + COL_Password + " TEXT, " + COL_Appreciation + " INTEGER);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + BOOK_TABLE_NAME);
        onCreate(sqLiteDatabase);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PROFILE_TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public Cursor getAllBooks(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM "+ BOOK_TABLE_NAME;
        Cursor res = db.rawQuery(query, null);
        return res;
    }

    public Cursor getBookPhotos(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COL_BookPhoto + " FROM "+ BOOK_TABLE_NAME;
        Cursor res = db.rawQuery(query, null);
        return res;
    }

    public Book getBookById(int id){
        String query = "SELECT * FROM " + BOOK_TABLE_NAME + " WHERE " + COL_BookId + " = \"" + id + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Book objBook = new Book();
        if(cursor.moveToFirst()){
            cursor.moveToFirst();
            objBook.setId(Integer.parseInt(cursor.getString(0)));
            objBook.setTitle(cursor.getString(1));
            objBook.setAuthor(cursor.getString(2));
            objBook.setDesc(cursor.getString(3));
            objBook.setDate(cursor.getString(4));
            objBook.setBookCat(cursor.getString(5));
            objBook.setBookPhoto(cursor.getBlob(6));
            cursor.close();
        }else {
            objBook = null;
        }
        db.close();
        return objBook;
    }

    public void addBook(Book book){
        ContentValues values = new ContentValues();
        values.put(COL_Title, book.getTitle());
        values.put(COL_Author, book.getAuthor());
        values.put(COL_Description, book.getDesc());
        values.put(COL_Date, book.getDate());
        values.put(COL_BookCat, book.getBookCat());
        values.put(COL_BookPhoto, book.getBookPhoto());
        Log.d(TAG, "addBook: " + "Added Book Photo" );
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(BOOK_TABLE_NAME, null, values);
        db.close();
    }

    public boolean deleteBookId(int id){
        boolean result = false;
        String query = "SELECT * FROM " + BOOK_TABLE_NAME + " WHERE " + COL_BookId + " = \"" + id + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query,null);
        Book bookobj = new Book();
        if (cursor.moveToFirst()){
            bookobj.setTitle(cursor.getString(1));
            db.delete(BOOK_TABLE_NAME, COL_BookId + " =?", new String[]{ String.valueOf(bookobj.getId())});
            cursor.close();
            result = true;
            Log.d(TAG, "deleteProductID: Item deleted");
        }
        else {
            Log.d(TAG, "deleteProductID: Item not deleted");
        }
        db.close();
        return result;
    }

    /*public void addBookPhoto(byte[] image){
        ContentValues cv = new ContentValues();
        cv.put(COL_BookPhoto, image);
        db.insert(BOOK_TABLE_NAME, null, cv);
        Log.d(TAG, "addBookPhoto: Successfully saved photo as byte[]");
    }*/

    //Profile database codes

    public Profile getProfileDetailsById(int id){

        String query = "SELECT * FROM " + PROFILE_TABLE_NAME + " WHERE " + COL_ProfileId + " = \"" + id + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Profile objProfile = new Profile();
        if(cursor.moveToFirst()){
            cursor.moveToFirst();
            objProfile.setId(Integer.parseInt(cursor.getString(0)));
            objProfile.setName(cursor.getString(1));
            objProfile.setPhoto(cursor.getBlob(2));
            objProfile.setEmail(cursor.getString(3));
            objProfile.setPhoneNo(cursor.getString(4));
            Log.d(TAG, "getProfileDetailsById: Profile is not Null");
        }
        else{
            objProfile = null;
            Log.d(TAG, "getProfileDetailsById: Profile is Null");
        }
        db.close();
        return objProfile;
    }

    public void CreateProfile(Profile objProfile){
        ContentValues values = new ContentValues();
        values.put(COL_Name, objProfile.getName());
        values.put(COL_Photo, objProfile.getPhoto());
        values.put(COL_Email, objProfile.getEmail());
        values.put(COL_PhoneNo, objProfile.getPhoneNo());
        values.put(COL_Password, objProfile.getPassword());
        values.put(COL_Appreciation, objProfile.getAppreciation());
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(PROFILE_TABLE_NAME, null, values);
        Log.d(TAG, "CreateProfile: Created Profile" + objProfile.getName() + objProfile.getPhoneNo() + objProfile.getEmail() + objProfile.getPassword() + objProfile.getAppreciation());
        db.close();
    }

    public Profile getProfile(String Email, String Password){
        String query = "SELECT * FROM " + PROFILE_TABLE_NAME + " WHERE " + COL_Email + " = \"" + Email + "\"" + " AND " + COL_Password + " = \"" + Password + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Profile objProfile = new Profile();
        if(cursor.moveToFirst()){
            cursor.moveToFirst();
            objProfile.setId(Integer.parseInt(cursor.getString(0)));
            objProfile.setName(cursor.getString(1));
            objProfile.setPhoto(cursor.getBlob(2));
            objProfile.setEmail(cursor.getString(3));
            objProfile.setPhoneNo(cursor.getString(4));
            objProfile.setPassword(cursor.getString(5));
            objProfile.setAppreciation(Integer.parseInt(cursor.getString(6)));
            Log.d(TAG, "getProfile: " + objProfile.getAppreciation());
            Log.d(TAG, "getProfile: Profile is not Null");
        }
        else{
            objProfile = null;
            Log.d(TAG, "getProfile: Profile is Null");
        }
        db.close();
        return objProfile;
    }

    public Boolean checkProfileExist(String Email, String Password){
        String query = "SELECT * FROM " + PROFILE_TABLE_NAME + " WHERE " + COL_Email + " = \"" + Email + "\"" + " AND " + COL_Password + " = \"" + Password + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Profile objProfile = new Profile();
        if(cursor.moveToFirst()){
            db.close();
            return true;
        }
        else{
            objProfile = null;
            db.close();
            return false;
        }

    }

    public Boolean updateProfilePhoto(int Id, String Name, byte[] Photo, String Email, String PhoneNo, int Appreciation){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_Name, Name);
        contentValues.put(COL_Photo, Photo);
        contentValues.put(COL_Email, Email);
        contentValues.put(COL_PhoneNo, PhoneNo);
        contentValues.put(COL_Appreciation, Appreciation);
        String id = Integer.toString(Id);
        db.update(PROFILE_TABLE_NAME, contentValues, "ProfileId = ?", new String[] { id });
        return true;

        /*String query = "UPDATE " + PROFILE_TABLE_NAME + " SET " + COL_Name + " = \"" + Name + "\", " + COL_Photo + " = \"" + Photo + "\", " + COL_Email + " = \"" + Email + "\", " + COL_PhoneNo + " = \"" + PhoneNo + "\" WHERE " + COL_ProfileId + " = \"" + Id + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        db.rawQuery(query, null);
        db.close();*/
    }

    public Boolean updateProfileDetails(int Id, String Name, String Email, String PhoneNo, int Appreciation){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_Name, Name);
        contentValues.put(COL_Email, Email);
        contentValues.put(COL_PhoneNo, PhoneNo);
        contentValues.put(COL_Appreciation, Appreciation);
        String id = Integer.toString(Id);
        db.update(PROFILE_TABLE_NAME, contentValues, "ProfileId = ?", new String[] { id });
        return true;
    }


}
