package com.example.nghsiaojiet.everyonelibrary;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.craftman.cardform.Card;
import com.craftman.cardform.CardForm;
import com.craftman.cardform.OnPayBtnClickListner;

public class CreditCardForm extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card_form);

        final CardForm cardForm = findViewById(R.id.cardForm);
        final TextView txtDes = findViewById(R.id.payment_amount);
        final Button btnPay = findViewById(R.id.btn_pay);

        txtDes.setText("$20");
        btnPay.setText(String.format("Pay %s", txtDes.getText()));
        cardForm.setPayBtnClickListner(new OnPayBtnClickListner() {
            @Override
            public void onClick(Card card) {
                if(card.getName().isEmpty() || card.getCVC().isEmpty() || card.getNumber().isEmpty()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(CreditCardForm.this).setTitle("Confirm before purchase")
                            .setMessage("Card number: " + card.getNumber() + "\n" + "Name under card: " + card.getName() + "\n" +
                                    "Card expiry date: " + card.getExpMonth() + "/" + card.getExpYear() + "\n" +
                                    "Card Cvv: " + card.getCVC());
                    builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            Toast.makeText(CreditCardForm.this, "Thank you for your purchase", Toast.LENGTH_LONG).show();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
                else{
                    Toast.makeText(CreditCardForm.this, "Please fill up all the information before purchasing", Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}
